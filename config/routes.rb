Rails.application.routes.draw do
  devise_for :models
  get 'welcome/index'

  resources :articles

  root 'welcome#index'
end
