class WelcomeController < ApplicationController
  def index
  end

  before_action :authenticate_model!
end
